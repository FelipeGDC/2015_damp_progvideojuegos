﻿using UnityEngine;
using System.Collections;

public class BallMovement : MonoBehaviour {

    public Transform paddleTr;
    private Vector3 paddleBallV3;
    private bool ballFree;
    private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        paddleBallV3 = transform.position - paddleTr.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (!ballFree) {
            transform.position = paddleTr.position + paddleBallV3;
            if (Input.GetMouseButtonDown(0)) {
                ballFree = true;
                rb.velocity = new Vector2(Random.Range(-10f, 10f), 7f);
            }
        } 
	}
}
