﻿using UnityEngine;
using System.Collections;

public class DieOnTriggerEnter : MonoBehaviour {

    public LevelManager levelMgr;
    
	
    void OnTriggerEnter2D(Collider2D col) {
        print("DieOnTrigger Triggered by: " + col.name);
        levelMgr.LoadLevel("Start Menu");
    }
}
