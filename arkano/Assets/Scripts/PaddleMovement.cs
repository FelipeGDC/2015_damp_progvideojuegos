﻿using UnityEngine;
using System.Collections;

public class PaddleMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 newPosition = new Vector3(
            Mathf.Clamp(Input.mousePosition.x / Screen.width * 16F - 8F, -7F, 7F),
            transform.position.y,
            transform.position.z);
        transform.position = newPosition;
        
    }
}
