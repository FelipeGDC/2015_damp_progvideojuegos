<!-- .slide: class="center" -->

# Desarrollo de Entornos Interactivos Multidispositivo  

<br>

#### 
#### Juan Raigada · Oscar Espeso Gil


-----

<!-- .slide: class="left" -->

Desarrollo de Entornos Interactivos Multidispositivo  
<!-- .element: class="header" -->

### Dos Unidades

* Programación en C# &rArr; [oscar.espeso@live.u-tad.com](mailto:oscar.espeso@live.u-tad.com)
	
	<!-- .element: class="fragment highlight-current-red" data-fragment-index="1"-->
	 
	* 55% de la asignatura
	* Fundamentos de Programación en Unity y C#
	* Prefijo de las entregas: **Prog_**
		* Ejemplo: `Prog_T2_03_Raycast`  
		  Traducción: Entrega 3 del segundo trimestre de la Unidad de Programación



* Proyecto de videojuego &rArr; [juan.raigada@live.u-tad.com](mailto:juan.raigada@live.u-tad.com) 
	
	<!-- .element: class="fragment fragment highlight-current-red"-->
	* 45% de la asignatura
	* Diseño, mecanicas e integración en Unity
	* Prefijo de las entregas: **Unity_**
		* Ejemplo: `Unity_T1_02_Prototipo_1`  
		  Traducción: Entrega 2 del primer trimestre de la Unidad de Proyecto de videojuego


-----

Desarrollo de Entornos Interactivos Multidispositivo  
<!-- .element: class="header" -->  
### Evaluación
* Continua en ambas Unidades
	* Se tienen en cuenta conocimientos, compentencias y la evolución a lo largo del curso
	* 60% actividades
	* 30% exámenes (si suspendes, suspendes la unidad)  
	* 10% actitud  <br><br>
	* No hay nota del trimestre del módulo. Hay nota trimestral orientativa de las unidades.

* La nota final del módulo es  
	* 55% Programación
	* 45% Proyecto
	
* Si suspendes una unidad suspendes el módulo &rArr; Convocatoria Extraordinaria &rArr; Prácticas en septiembre
<!-- .element: class="fragment fragment highlight-current-red grow"-->
	
	
-----

<!-- .slide: class="center" -->

## Unity
---
# Programación en
# C**#** 
---
## DEIM - Prog

-----


Programación en C#
<!-- .element: class="header" -->


<!-- .slide: class="center" -->
# ¿Por qué C_#_?



----


#### *¿Por qué C_#_?*
---
### Malas razones:

* Porque C# es más rápido que UnityScript &rArr; Es un mito	
<!-- .element: class="fragment left" -->
  * Unity produce código nativo antes de compilar definitivamente el juego. 	
	  Da igual (o casi) que lenguaje hayamos usado originalmente. 
	  <!-- .element: class="fragment" -->	

* Porque Javascript es un lenguaje de juguete, el hermano tonto de Java. 	
<!-- .element: class="fragment "-->

	* En Unity no hay Javascript, hay UnityScript (el año pasado hacíamos trampa) 	
	<!-- .element: class="fragment " -->
	
	* Javascript es un lenguaje mucho más potente de lo que la gente piensa.	
	<!-- .element: class="fragment "-->

		* Java y C# han copiado a Javascript. 	
		<!-- .element: class="fragment "-->


----


#### *¿Por qué C_#_?*
---
### Buenas razones:


* Es mucho más estricto &rArr; Más fácil localizar errores 
<!-- .element: class="fragment  " -->

* Como lenguaje es más completo. Tiene muchas características y herramientas que no existen en UnityScript 
<!-- .element: class="fragment  " -->

* Se utiliza para muchas más cosas que Unity 
<!-- .element: class="fragment  " -->

* Toda la documentación de Unity está escrita pensando en C# 
<!-- .element: class="fragment  " -->

* La comunidad de Unity usa C# &rArr; Foros, Unity Answers, tutoriales,... 
<!-- .element: class="fragment  " -->





-----

<!-- .slide: class="center" -->

# El juego de las 7 diferencias
## entre Javascript y C_#_


----


*Diferencias entre Javascript y C#*

---


#### Scripts &hArr; Clases
<!-- .element: class="fragment"-->

```javascript  
// En javascript
var vel = 4.0;
function Update() {
	transform.position += transform.right * vel * Time.deltaTime;
}
``` 
<!-- .element: class="fragment "-->
	
```cs
// En C#
using UnityEngine;
using System.Collections;

public class PlayerBehaviour : MonoBehaviour {

	public float vel = 4f;
	void Update() {
		transform.position += transform.right * vel * Time.deltaTime;
	}
	
}
```
<!-- .element: class="fragment "-->

----


*Diferencias entre Javascript y C#*

---

* Decíamos que un script era un *componente*. 
<!-- .element: class="fragment "-->

* Un *componente* es una *clase* de objeto.
<!-- .element: class="fragment "-->

* En C# tenemos que expresarlo explícitamente (en UnityScript no hacía falta).
<!-- .element: class="fragment"-->

* El código que antes simplemente escribíamos en un script, ahora hay que encerrarlo en la siguiente estructura (que va a ser casi siempre igual):
<!-- .element: class="fragment"-->

```cs
using UnityEngine;
using System.Collections;
public class PlayerBehaviour : MonoBehaviour {

	// Aquí dentro va el script en C#
}
```
<!-- .element: class="fragment"-->

----



*Diferencias entre Javascript y C#*

---

#### Declaración de variables:

Antes en javascript:

```js
var vel = 4.0;                  
```
<!-- .element: class="fragment"-->

Ahora en C#:
<!-- .element: class="fragment"-->

```csharp
public float vel = 4f;
```
<!-- .element: class="fragment whole-line"-->
* Tres conceptos nuevos: 
<!-- .element: class="fragment"-->

  	* Atributo de acceso (`public`) 
  <!-- .element: class="fragment"-->  
  	* Tipo de variable (`float`) 
  <!-- .element: class="fragment"-->  
  	* Tipo de dato o valor (la `f` en `4f`)
  <!-- .element: class="fragment"-->  


----



*Diferencias entre Javascript y C#*

---

#### Funciones:

Antes, en javascript:
<!-- .element: class="fragment"-->  

```js
function fireBullets(bulletPrefab) {
	var bulletsCount;
	while (onSight) {
		Instantiate(bulletPrefab);
		bulletsCount++;
	}
	return bulletsCount;
}

function OnTriggerStay(targetCollider) {
	aimOnTarget(targetCollider);
	var bulletsLeft = ammo - fireBullets(explosiveBulletPrefab);
	updateAmmoHUD(bulletsLeft);
}
```
<!-- .element: class="fragment"-->  

**Importante:** Este código no funciona. Es sólo para mostrar las diferencias.

<!-- .element: class="fragment"--> 

----



*Diferencias entre Javascript y C#*

---

#### Funciones:

Ahora, en C#

```csharp
private int fireBullets(GameObject BulletPrefab) {
	int bulletsCount;
	while (!onSight) {
		Instantiate(bulletPrefab);
		bulletsCount++;
	}
	return bulletsCount;
}

void OnTriggerStay(Collider targetCollider) {
	aimOnTarget(targetCollider);
	int bulletsLeft = ammo - fireBullets(explosiveBulletPrefab);
	updateAmmoHUD(bulletsLeft);
}

```

**Importante:** Este código tampoco funciona. Es sólo para mostrar las diferencias.

<!-- .element: class="fragment"--> 


----

<!-- .slide: class="center" -->

*Diferencias entre Javascript y C#*

---

#### Funciones:
 
```js
// En Javascript

function fireBullets(bulletPrefab) {
	var bulletsCount;
	while (onSight) {
		Instantiate(bulletPrefab);
		bulletsCount++;
	}
	return bulletsCount;
}

function OnTriggerStay(targetCollider) {
	aimOnTarget(targetCollider);
	var bulletsLeft = ammo - fireBullets(explosiveBulletPrefab);
	updateAmmoHUD(bulletsLeft);
}
```
<!-- .element: class="comparison"-->  

```csharp
// En C# 

private int fireBullets(GameObject BulletPrefab) {
	int bulletsCount;
	while (onSight) {
		Instantiate(bulletPrefab);
		bulletsCount++;
	}
	return bulletsCount;
}

void OnTriggerStay(Collider targetCollider) {
	aimOnTarget(targetCollider);
	int bulletsLeft = ammo - fireBullets(explosiveBulletPrefab);
	updateAmmoHUD(bulletsLeft);
}

```
<!-- .element: class="comparison"-->

**Importante:** Ninguno de estos dos códigos funciona. Son sólo para mostrar las diferencias entre Javascript y C#.

<!-- .element: class="fragment"--> 


----

<!-- .slide: class="left" -->

*Diferencias entre Javascript y C#*

<!-- .element: class="center" -->

--- 

#### Definición de funciones:

* En C# **no** se usa la palabra clave **`function`**
<!-- .element: class="fragment"-->  

* En C# es obligatorio expresar:
<!-- .element: class="fragment"-->  
	* El atributo de acceso de la función (`private`, `public`, ...)
	<!-- .element: class="fragment"-->  
	
	* De qué tipo son los parámetros de la función (`int`, `Collider`, `GameObject`, etc.)
	<!-- .element: class="fragment"-->  
	
	* De qué tipo es el valor que devuelve la función (... además `void`)
	<!-- .element: class="fragment"-->  
	
<br>
#### Llamada a funciones:
<!-- .element: class="fragment"--> 

* Es exáctamente igual en los dos lenguajes
<!-- .element: class="fragment"--> 
