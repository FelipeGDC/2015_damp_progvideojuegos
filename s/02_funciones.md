## Funciones como definición
(devuelven un valor, definen como se calcula ese valor, ese algoritmo es el concepto)

¿Quién es el presidente?
`Presidente = "mariano Rajoy";`
La función define al presidente
```csharp
string electedpresident (string[] votes) {
	int votes4Sanchez;
	int votes4Rajoy;
	int votes4Iglesias;
	int votes4Garzon;
	// ...
	for (int i = 0 ; i < votos.length ; i++) {
		if (votes[i] == "Sánchez") { 
			votes4Sanchez++ 
		}
		if (votes[i] == "Rajoy") { 
			votes4Rajoy++ 
		}
		if (votes[i] == "Iglesias") { 
			votes4Iglesias++ 
		}
		if (votes[i] == "Garzón") { 
			votes4Garzon++ 
		}
		// ...
		
		int[] votesCount = [votes4Sanchez, votes4Rajoy, votes4Iglesias, votes4Garzon];
		string[] candidates = ["Iglesias", "Rajoy", "Sánchez", "Garzón"];
		
		int maxVotes = votesCount.Max();
		
		string winner = candidates[votesCount.IndexOf(maxVotes)];
		
		return winner;
		
	}

}


var president = electedPresident(["Sánchez", "Garzón", "Rajoy", "Rajoy", "Iglesias", 
			"Rajoy", "Garzón", "Sánchez", "Rajoy", "Iglesias", ... ]);

```
